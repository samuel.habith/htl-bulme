{#
Using extends instead of normal layout chaining is necessary here as
most of the content is shared between multiple unit test intros for different
programming languages with only a few blocks being distinct.
#}

{% extends "layouts/presentation.njk" %}
{% set source_path = "source/" + language_ + "/" %}
{% set data_path = "/data/" + language_ + "/" %}
{% set compiler = clang %}
{% set alt_compiler = gcc %}

{% block presentation %}

<section>
  <h2>Compiling a Basic Program</h2>
{% highlight language_ %}
{% include source_path + "hello." + language_ %}
{% endhighlight %}
  <p>{% download data_path + "hello." + language_ %}</p>
  <p>Compilation can be done with either <code>{{ clang }}</code> or
    <code>{{ gcc }}</code> (part of the GNU Compiler Collection, GCC).</p>
{% highlight "bash" %}
{{ compiler }} $INFILE -o $PROGRAM_NAME
{{ alt_compiler }} $INFILE -o $PROGRAM_NAME
{% endhighlight %}
</section>

<section>
  <h2>I/O</h2>
  <section>
    <h3>Writing to Standard Output</h3>
{% block cpp_io_streams %}{% endblock %}
{% highlight language_ + " 0,4" %}
{% include source_path + "hello." + language_ %}
{% endhighlight %}
{% include_code source_path + "hello." + language_ %}
  </section>

  <section>
    <h3>Reading Input</h3>
{% highlight language_ + " 0,6"%}
{% include source_path + "input." + language_ %}
{% endhighlight %}
    <p>{% download data_path + "input." + language_ %}</p>
  </section>
</section>

<section>
  <h2>Variables and Data Types</h2>
  <section>

{% block cpp_initialization %}{% endblock %}

{% highlight language_ %}
{% include source_path + "data_types." + language_ %}
{% endhighlight %}
{% include_code source_path + "data_types." + language_ %}
  </section>

  <section>
    <h3>Block Scope</h3>
    <p>Identifiers are only valid in their defining block</p>
{% highlight language_ %}
{% include source_path + "block_scope." + language_ %}
{% endhighlight %}
    <p>{% download data_path + "block_scope." + language_ %}</p>
  </section>

  {% block cpp_types %}{% endblock %}
</section>

<section>
  <h2>Conditional Execution</h2>

  <section>
    <h3><code>if</code> and <code>else</code></h3>
{% highlight language_ %}
if (expression) {
  // block
} else if (expression) {
  // block
} else {
  block
}
{% endhighlight %}
    <div class="fragment">
    <p>The <a href="https://www.freecodecamp.org/news/c-ternary-operator/">ternary
      operator</a> provides a shorthand</p>
{% highlight language_ %}
Type result = condition ? value_if_true : value_if_false;
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example: <code>if</code> and <code>else</code></h3>
{% highlight "c 4,6,8" %}
{% include source_path + "if." + language_ %}
{% endhighlight %}
{% include_code source_path + "if." + language_ %}
  </section>

  <section>
    <h3><code>switch</code></h3>
    <p>The {{ language }} switch statement jumps to the block of code matching the value
      of an expression.<br />
      It executes one or multiple code blocks among many alternatives.</p>
{% highlight language_ %}
switch (expression) ​{
    case constant1:
      // statements
      break;  // without break, execution of the following blocks continues
    case constant2:
      // statements
      break;
    // ...
    default:
      // statements
}
{% endhighlight %}
  </section>

  <section>
    <h3>Example: <code>switch</code></h3>
    <p></p>
{% highlight language_ + " 2,3,5,6,9,12"%}
{% include source_path + "switch." + language_ %}
{% endhighlight %}
{% include_code source_path + "switch." + language_ %}
  </section>

</section>

<section>
  <h2><code>while</code> Loops</h2>

  <section>
{% highlight language_ %}
while (expression) {
  // block
}
{% endhighlight %}
    <div class="fragment">
      <p><code>do ... while</code>: run code block at least once
        regardless of the expression</p>
{% highlight language_ %}
do {
  // block
} while (expression);
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example</h3>
{% highlight "c 4-7" %}
{% include source_path + "while." + language_ %}
{% endhighlight %}
{% include_code source_path + "while." + language_ %}
  </section>

  <section>
    <h3>Example: Execute a block of code at least once</h3>
    {% highlight language_ + " 4,6" %}
    {% include source_path + "do." + language_ %}
    {% endhighlight %}
    {% include_code source_path + "do." + language_ %}
  </section>
</section>

<section>
  <h2><code>for</code> Loops</h2>

  <section>
{% highlight "c 4-7" %}
for (initialization; condition; update_statement) {
  // block
}
{% endhighlight %}
    <div class="fragment">
      <p><code>initialization</code> and <code>update_statement</code> are optional</p>
    </div>
    <div class="fragment">
      <p>Every <code>for</code> loop can be expressed as <code>while</code>
        loop</p>
{% highlight language_ %}
// initialization
for (; condition; ) {
  // block
  // update_statement;
}

while (condition) {
  // block
}
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Example</h3>
{% highlight language_ %}
{% include source_path + "for." + language_ %}
{% endhighlight %}
{% include_code source_path + "for." + language_ %}
  </section>

  {% block cpp_for %}
  {% endblock %}
</section>

<section>

  <section>
    <h2>Function Signature</h2>
    <p>aka. <strong>type signature</strong> or
      <strong>type annotation</strong></p>
    <p>Defines the data types of the parameters and return value</p>
    <div class="fragment">
      <p>For example, a function that returns the sum of two integers:</p>
{% highlight language_ %}
(int)(int, int)
{% endhighlight %}
    </div>
  </section>

  <section>
    <h2>Function Declaration</h2>
    <p>aka. <strong>function prototype</strong> or
      <strong>function interface</strong></p>
      <ul>
        <li>Specifies the function name and type signature, but omits the
          body</li>
        <li>Required for using functions that are defined elsewhere</li>
        <li>Promise to the compiler that the function will exist when linking</li>
      </ul>
      <p></p>
    <div class="fragment">
      <p>For example, a function that returns the sum of two integers:</p>
{% highlight language_ %}
int sum(int a, int b);
{% endhighlight %}
    </div>
    <div class="fragment">
      <p>The parameter names are optional:</p>
{% highlight language_ %}
int sum(int, int);
{% endhighlight %}
    </div>
  </section>

  <section>
    <h2>Function Definition</h2>
    <ul>
      <li>Adds the function body to the declaration</li>
      <li>The signature and name must exactly match the declaration</li>
    </ul>
{% highlight language_ %}
Type function_name(Type parameter1, Type parameter2, ...) { body }
{% endhighlight %}
    <div class="fragment">
      <p>For example, a function that returns the sum of two integers:</p>
{% highlight language_ %}
int sum(int a, int b) {
  return a + b;
}
{% endhighlight %}
    </div>
  </section>

  <section>
    <h2>Pass by Value</h2>
    <p>By default, {{ language }} copies argument values to function
      parameters</p>
{% highlight language_ + " 2,10" %}
{% include source_path + "by_value." + language_ %}
{% endhighlight %}
{% include_code "source/cpp/by_value.cpp" %}
  </section>

{% block cpp_functions %}{% endblock %}
</section>

{% block cpp_content %}{% endblock %}

{% include 'slides/closing.njk' %}
{% endblock %}
