Slide decks for

* arrays - introduce for loops for iteration
* first steps: text editor, main, data types, and operators + precedence (solve a concrete problem)
* functions, parameters, return; omitting return -> void
  - variadic functions
* debugging c programs (commandline debugger lldb / gdb + ide incl. config)
* data structures in C (self-made containers: linked list, doubly linked list,
  binary trees, ...)
* enum (and union) (maybe add to struct presentation)
* doxygen syntax and generation of documentation

* not C specific: running unit tests in version control (example
  gitlab / github)
* recursion (write as template to be used in multiple programming languages)

## variadic functions
  Variadic functions are functions which take a variable number of arguments. In C programming, a variadic function will contribute to the flexibility of the program that you are developing.

The declaration of a variadic function starts with the declaration of at least one named variable, and uses an ellipsis as the last parameter, e.g.

int printf(const char* format, ...);
https://en.wikipedia.org/wiki/Variadic_function#In_C 

---
  Flow control—flow control and loops
  Functions—user-defined, standard library, and recursion
  Arrays—fixed-size arrays, pointers, and variable-sized arrays
  Files—reading and writing files
  Console programming—conio
  Putting it together—writing a turn-based game

## presentation on string parsing security in C
scanf - why is it bad? improvements and alternatives
sscanf - what would be better?
atoi - what's wrong here?
strtol - best in stdlib, but not for complete parsing.
issue => do it youself to be safe...

// https://stackoverflow.com/a/9245829/104659


#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
    const char *s = "999999999999999999999999999";
    int n;
    sscanf(s, "%d", &n);
    printf("n = %d\n", n);
    int m = atoi(s);
    printf("m = %d\n", m);
    long q = strtol(s, NULL, 10);
    printf("q = %ld\n", q);
    printf("LONG_MAX: %ld\n", LONG_MAX);
    return 0;
}
