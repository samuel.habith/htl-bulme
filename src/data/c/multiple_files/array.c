#include "array.h"  // local includes use quotation marks

#include <stdlib.h>

int shared = 42;

/*
 * Return an array range of integers from 0 to dimension - 1.
 * The array must be freed after use.
 */
int *arange(size_t dimension) {
  int *array = (int*) malloc(dimension * sizeof(int));
  for (size_t i = 0; i < dimension; ++i) {
    array[i] = (int) i;
  }
  return array;
}
