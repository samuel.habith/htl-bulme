#include <stdlib.h>

#include "ui.h"  // local includes use quotation marks
#include "array.h"

int main(int argc, char** argv) {
  size_t dimension = get_dimension(argc, argv);
  int *array = arange(dimension);
  print_shared();
  print_array(array, dimension);
  return 0;
}
