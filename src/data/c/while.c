#include <stdio.h>

int main(void) {
  int n = 10;
  while (n > 0) {  // execute block while expression evaluates to `true`
    printf("%d, ", n);
    --n;  // avoid side effects in statement above
  }
  printf("FIRE!\n");
  return 0;
}
