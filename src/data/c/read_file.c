#include <stdio.h>

int main(void) {
  FILE* fp = fopen(__FILE__, "r");  // current source filename at compile time
  if (fp == NULL) {
    printf("Cannot open file '%s'\n", __FILE__);
    return 1;
  }

  // Read contents from file
  char c = fgetc(fp);
  while (c != EOF) {
      printf("%c", c);
      c = fgetc(fp);
  }
  fclose(fp);
  return 0;
}
