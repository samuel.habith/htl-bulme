#include <stdio.h>

int main() {
    int a = 5;
    int* a_ptr = &a;  // &: address-of operator
    *a_ptr += 5;  // *: dereference operator
    printf("updated value: %d (at %p - %zu bytes)\n", a, a_ptr, sizeof(a_ptr));
    return 0;
}
