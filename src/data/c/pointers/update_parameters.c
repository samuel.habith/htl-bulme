#include <stdio.h>

void update(int* a_ptr,int *b_ptr) {
  *a_ptr += 10;  // assign new value to memory address of `a`
  *b_ptr = 100;  // assign new value to `b`
  // still possible to return a value if needed
}

int main() {
  int a=4;
  int b=5;
  int* a_ptr = &a;  // &: address-of operator
  update(a_ptr, &b);
  printf("updated values: %d %d\n", a, b);
  return 0;
}
