#include <stdio.h>
#include <ctype.h>

int main() {
  char input_text[] = "The world is beautiful!";
  unsigned short index = 0;
  while (input_text[index]) {  // while char is not '\0'
    input_text[index] = toupper(input_text[index]);
    index++;
  }
  printf("changed input_text: `%s`\n", input_text);
  return 0;
}
