#include <stdio.h>

int main() {
  char text[] = "Freedom";
  size_t index = 0;
  while (text[index]) {
    printf("%c dec:%03d (%p)\n", text[index], text[index],
           &text[index]);
    index++;
  }
}
