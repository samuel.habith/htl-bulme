#include <stdio.h>
#include <stdlib.h>

int main() {
  FILE* f = fopen("data.txt", "a");  // open for appending

  if(f == NULL) {  // we have to check if opening was successful
    fprintf(stderr, "Error!");
    return EXIT_FAILURE;
  }

  fprintf(f, "%.2lf\n", 36.8);
  fprintf(f, "Additional text\n");
  fclose(f);

  return 0;
}
