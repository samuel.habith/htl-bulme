#include <stdio.h>

int main() {
  size_t count = 5;  // entered by the user
  unsigned int squares[count];
  for (size_t i = 0; i < count; ++i) {
    squares[i] = i * i;
  }
  for (size_t i = 0; i < count; ++i) printf("%u\n", squares[i]);
  return 0;
}
