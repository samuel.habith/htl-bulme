#include <stdio.h>

typedef struct {
  float x;
  float y;
} Point;

void print_point(Point p) {
  printf("P(%.2f/%.2f)\n", p.x, p.y);
  p.x = 0;  // does not affect p in main
}

int main() {
  Point p = { .x=1.5 , .y=3.0 };
  print_point(p);  // pass by value
  return 0;
}
