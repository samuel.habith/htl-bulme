#include <stdio.h>

int sum(int a, int b) { return a + b; }
int main(void) {
  int (*sum_ptr)(int,int);  // declares a function pointer named `sum_ptr`
  sum_ptr = sum;
  printf("The sum of 2 + 3 is %d\n", sum_ptr(2, 3));
  return 0;
}
