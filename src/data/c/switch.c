#include <stdio.h>
int main(int argc, char* argv[]) {
  switch (argc) {  // switch evaluates an expression: (argc)
  case 1:  // if the result of the expression evaluates to 1, jump here
    printf("Only the command was entered.\n");
    break;  // break - jump out of the 'switch' block to avoid falltrough
  case 2:
    printf("Command plus one argument.\n");
    break;
  case 3:
    printf("Command plus two arguments.\n");
    break;
  default:  // any other value of the expression jumps here
    printf("Command plus %d arguments.\n", argc-1); break;
  }
  return 0;
}
