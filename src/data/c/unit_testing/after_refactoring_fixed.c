// Return the sum of all values from 1 to n.
unsigned long long add_to(unsigned n) {
    return (unsigned long long) ((n / 2.0 * n) + (n / 2.0));
}
