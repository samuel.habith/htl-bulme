// Return the sum of all values from 1 to n.
unsigned long long add_to(unsigned n) {
    unsigned long long sum = 0;
    for (unsigned i = 1; i <=n; ++i) {
        sum += i;
    }
    return sum;
}
