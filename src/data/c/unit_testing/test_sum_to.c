#include <criterion/criterion.h>
#include <criterion/new/assert.h>
unsigned long long add_to(unsigned n);  // usually in header file

Test(sum_to, fast_test) {
  cr_assert(eq(u64, add_to(0), 0), "Sum of 0 is 0.");
  cr_assert(eq(u64, add_to(25), 325), "Sum of 1..25 is 325.");
  cr_assert(eq(u64, add_to(26), 351), "Sum of 1..26 is 351.");
  cr_assert(eq(u64, add_to(500000000), 125000000250000000),
    "This function must work for large unsigned integers.");
}
// does not terminate for the old code version due to an overflow of the
// loop counter (the loop would terminate only at UINT_MAX + 1)
Test(sum_to, slow_test) {
  cr_assert(eq(u64, add_to(4294967295), 9223372034707292160),
    "The function must work for the largest unsigned integer.");
}
