#include <stdio.h>
#include <unistd.h>  /* for getopt */
int main (int argc, char **argv) {
  int c;
  int aopt = 0, bopt = 0;  // flags
  char *copt = 0, *dopt = 0;  // option arguments
  while ((c = getopt(argc, argv, "abc:d:")) != -1) {
    switch (c) {
    case 'a':
      aopt = 1;
      break;
    case 'b':
      bopt = 1;
      break;
    case 'c':
      copt = optarg;
      break;
    case 'd':
      dopt = optarg;
      break;
    case '?':
      break;
    default:
      printf ("?? getopt returned character code 0%o ??\n", c);
    }
  }
  if (optind < argc) {
      printf ("non-option ARGV-elements: ");
      while (optind < argc) {
          printf ("%s ", argv[optind++]);
      }
      printf ("\n");
  }
  printf ("aopt: %d, bopt: %d, copt: %s, dopt: %s\n",
    aopt, bopt, copt, dopt);
  return 0;
}
