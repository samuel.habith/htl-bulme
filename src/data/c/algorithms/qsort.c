#include <stdio.h>
#include <stdlib.h> // qsort
int compare (const void * a, const void * b) {
  return ( *(int*)a - *(int*)b );
}
int main () {
  int values[] = { 33, 12, 99, 90, 7, 45 };
  qsort (values, 6, sizeof(int), compare);
  for (size_t n=0; n<6; ++n) {
     printf ("%d ",values[n]);
  }
  printf("\n");
  return 0;
}
