#include <stdio.h>
#include <stdlib.h>

int main(void) {
  size_t size = 8192 * 1024;  // 8 MB
  int* array = (int*) malloc(size);
  if (array == NULL) {
    fprintf(stderr, "malloc %lu bytes failed\n", size);
    exit(EXIT_FAILURE);
  }
  for (size_t i = 0; i < size / sizeof(int); ++i) {
    array[i] = i * i;  // initialize the array ...
  }
  printf("Survived?\n");
  free(array);  // avoid memory leak
  return 0;
}
