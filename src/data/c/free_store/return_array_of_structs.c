#include <stdio.h>
#include <stdlib.h>

typedef struct point { double x; double y; } Point;

Point* fetch_points() {
  Point* points = (Point*) malloc(2 * sizeof(Point));
  points[0] = (Point) { .x=4, .y=7 };
  points[1] = (Point) { .x=-5, .y=3 };
  return points;
}

int main(void) {
  Point* points = fetch_points();
  if (points == NULL) {
    fprintf(stderr, "no memory allocated\n"); exit(EXIT_FAILURE);
  }
  printf("Point 1: %5.2lf / %5.2lf\n", points[0].x, points[0].y);
  printf("Point 2: %5.2lf / %5.2lf\n", points[1].x, points[1].y);
  free(points);
  return 0;
}
