#include <stdio.h>

int main(void) {
  char line[20];
  int number = 0;  // safer to initialize values
  printf("Enter an integer between 1 and 6 (inclusive): ");
  fgets(line, sizeof(line), stdin);
  sscanf(line, "%d", &number);
  printf("You've entered the number %d.\n", number);
  return 0;
}
