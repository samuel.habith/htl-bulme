#include <iostream>  // allows using std::cout, std::cin and std::endl

// main function - C++ programs start their execution here
int main(void) {
  int number;
  std::cout << "Please enter an integer: ";  // writing to stdout
  std::cin >> number;  // reading user input
  std::cout << "You've entered " << number << std::endl;
  return 0;  // indicate successful termination to environment
}  // blocks are delimited with curly braces
