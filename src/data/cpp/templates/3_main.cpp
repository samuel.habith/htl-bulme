#include <iostream>
#include <iomanip>
#include "3_function_template.hpp"

int main() {
  std::cout << std::fixed << std::setprecision(2);
  std::cout << max<int>(5, 6) << std::endl;  // calls int max(int, int)
  std::cout << max(1, 3) << std::endl;  // calls int max(int, int)
  std::cout << max<>(1.2, 3.4) << std::endl;  // calls max(double, double)
  std::cout << max<double>(5, 6) << std::endl; // calls max(double, double)
  std::cout << max('a', 'b') << std::endl;
  return 0;
}
