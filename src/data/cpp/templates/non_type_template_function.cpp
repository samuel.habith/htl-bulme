#include <array>
#include <iostream>
// compile via `clang std=c++17`
template <typename T, std::size_t size> // parameterize the element type and size
void print_array(const std::array<T, size>& myArray)
{
    for (auto element : myArray)
        std::cout << element << ' ';
    std::cout << '\n';
}

int main()
{
    std::array<double, 5> a { 9.0, 7.2, 5.4, 3.6, 1.8 };
    print_array(a);

    std::array b{ 9.0, 7.2, 5.4, 3.6, 1.8 };  // requires C++17
    print_array(b);

    return 0;
}
