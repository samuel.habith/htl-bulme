#ifndef FUNCTION_TEMPLATE_HPP
#define FUNCTION_TEMPLATE_HPP
template <typename T>
T max(T x, T y);

#endif  // FUNCTION_TEMPLATE_HPP
