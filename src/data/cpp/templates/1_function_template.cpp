#include "1_function_template.hpp"

template <typename T>
T max(T x, T y) {
  return (x >= y) ? x : y;
}
