#include "2_function_template.hpp"

template <typename T>
T max(T x, T y) {
  return (x >= y) ? x : y;
}
// tell the compiler to create the following types
template int max(int x, int y);
template double max(double x, double y);
