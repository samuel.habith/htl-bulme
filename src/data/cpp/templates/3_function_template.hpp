#ifndef FUNCTION_TEMPLATE_HPP
#define FUNCTION_TEMPLATE_HPP

template <typename T>
T max(T x, T y);

// definition in header file
template <typename T>
T max(T x, T y) {
  return (x >= y) ? x : y;
}

#endif  // FUNCTION_TEMPLATE_HPP
