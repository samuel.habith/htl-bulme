#include <iostream>
#include "1_function_template.hpp"
// In order to generate the code, the compiler must see the template definition
// (the declaration isn't sufficient) and the specific types used in the
// template. Is this the case here?
int main() {
    std::cout << max(5, 6) << std::endl;  // calls max(int, int)
    std::cout << max(1.2, 3.4) << std::endl;  // calls max(double, double)
    return 0;
}
