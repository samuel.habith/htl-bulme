#include <iostream>

int main() {
  bool b = true;  // c-like initialization
  char c('x');  // constructor initialization (only valid during declaration)
  int i {-5};  // uniform initialization (only valid during declaration)
  std::cout << b << std::endl;
  std::cout << c << std::endl;
  std::cout << i << std::endl;
  return 0;
}
