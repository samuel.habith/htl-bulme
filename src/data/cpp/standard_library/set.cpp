#include <algorithm>
#include <iostream>
#include <set>

void print_set(std::set<int>& s) {
  std::cout << "{";
  for (auto i : s) std::cout << i << ", ";
  std::cout << "}" << std::endl;
}

int main() {
  std::set<int> s1 {1, 4, 5, 3, 1, 3, 5, 4, 3, 1, 3, 4};  // => {1, 3, 4, 5}
  s1.insert(2);  // insert 2 into the set
  s1.insert(5);  // no element will be inserted
  std::cout << "size: " << s1.size() << std::endl;
  std::set<int> s2 {7, 8, 3, 9, 2, 6};

  std::set<int> intersection;
  std::ranges::set_intersection(s1, s2,
    std::inserter(intersection, intersection.begin()));
  print_set(intersection);

  std::set<int> union_;
  std::ranges::set_union(s1, s2,
    std::inserter(union_, union_.begin()));
  print_set(union_);

  return 0;
}
