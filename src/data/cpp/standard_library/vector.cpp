#include <iostream>
#include <vector>

void print_vector(const std::vector<int>& v) {  // using a reference avoids copying
  std::cout << "{";
  for (auto it(v.cbegin()); it != v.cend(); ++it) {
    std::cout << *it;
    if (it + 1 != v.cend()) std::cout << ", ";
  }
  std::cout << "}" << std::endl;;
}
int main() {
  std::vector<int> v{7, 5, 16, 8};
  v.push_back(1);  // add element to end of vector
  v.push_back(3);
  print_vector(v);  // the "magic" of references
}
