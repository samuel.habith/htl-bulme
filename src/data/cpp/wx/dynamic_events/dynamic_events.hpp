#ifndef DYNAMIC_EVENTS_HPP
#define DYNAMIC_EVENTS_HPP

#include <wx/wx.h>

class MainFrame : public wxFrame {
public:
  MainFrame(const wxString& title);
private:
  wxSlider* slider = nullptr;
  wxTextCtrl* text = nullptr;

  void on_button_add_clicked(wxCommandEvent& evt);
  void on_button_remove_clicked(wxCommandEvent& evt);
  void on_slide_changed(wxCommandEvent& evt);
  void on_text_changed(wxCommandEvent& evt);
};

#endif // DYNAMIC_EVENTS_HPP
