#include <iostream>

int main() {
  bool condition = false;
  do {
    std::cout << "Do this at least once." << std::endl;
  } while (condition);
  std::cout << "FIRE!" << std::endl;
  return 0;
}
