#include <iostream>
struct Point {
  int x = 0;
  int y = 0;
  void print() {
    std::cout << "x: " << (*this).x << ", y: " << this->y << std::endl;
  }
};
int main() {
  Point p{ .x = 5, .y = 3 };  // initialize with given values
  p.print();
  return 0;
}
