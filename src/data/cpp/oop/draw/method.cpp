#include <iostream>
struct Point {
  int x = 0;  // default values are allowed since C++11
  int y = 0;
  void print() {
    std::cout << "x: " << x << ", y: " << y << std::endl;
  }
};
int main() {
  Point p{ .x = 5, .y = 3 };  // initialize with given values
  p.print();
  return 0;
}
