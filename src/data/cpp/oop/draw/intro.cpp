#include <iostream>
struct Point {
  int x = 0;  // default values are allowed since C++11
  int y = 0;
};
int main() {
  Point p;  // Create Point object (instance); initialize with default values
  std::cout << "x: " << p.x << ", y: " << p.y << std::endl;
  Point p2{ .x = 5, .y = 3 };  // Create another instance with given values
  p2.x = 4;  // members are public by default
  std::cout << "x: " << p2.x << ", y: " << p2.y << std::endl;
  return 0;
}
