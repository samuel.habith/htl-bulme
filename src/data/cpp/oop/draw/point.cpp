#include "point.hpp"
#include <iostream>

namespace draw {
Point::Point(int x, int y) : x(x), y(y) {};  // constructor within class scope
Point::Point() : Point(0, 0) {};  // Point() delegates to Point(0, 0)
void Point::print() {  // method within class scope
  std::cout << "x: " << x << ", y: " << y << std::endl;
};
}
