#ifndef INT_VECTOR_HPP
#define INT_VECTOR_HPP
#include <iostream>

namespace ds {
class IntVector {  // very simplified int vector class
public:
  ~IntVector();  // destructor
  IntVector(const IntVector&);  // copy constructor
  IntVector& operator=(const IntVector& other);  //copy assignment
  friend std::ostream& operator<<(std::ostream& out, const IntVector& v);
  void push_back(int elem);
  int pop_back();
  std::size_t size() { return size_; }
private:
  std::size_t size_ = 0;
  std::size_t space_ = 0;
  int* elements_ = nullptr;
  void resize(std::size_t size);
};
}


#endif
