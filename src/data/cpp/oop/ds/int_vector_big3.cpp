#include "int_vector_big3.hpp"
#include <algorithm>
#include <iostream>

namespace ds {

IntVector::~IntVector() {  // destructor
  delete[] elements_;
}

IntVector::IntVector(const IntVector& other) {  // copy constructor
  size_ = other.size_;  // default behavior
  space_ = other.space_;  // default behavior
  // explicitly deal with heap memory (create an array and copy elements to it)
  elements_ = new int[space_];
  std::copy(other.elements_, other.elements_ + size_, elements_);
}

IntVector& IntVector::operator=(const IntVector& other) {  // copy assignment
  size_ = other.size_;  // default behavior
  space_ = other.space_;  // default behavior
  // explicitly deal with heap memory (replace existing array)
  delete[] elements_;
  elements_ = new int[space_];
  std::copy(other.elements_, other.elements_ + size_, elements_);
  return *this;
}

std::ostream& operator<<(std::ostream& out, const ds::IntVector& v) {
  out << '[';
  for (size_t i = 0; i < v.size_ - 1; ++i) {
    out << v.elements_[i] << ", ";
  }
  if (v.size_) out << v.elements_[v.size_ - 1];
  out <<  ']';
  return out;
}

void IntVector::resize(std::size_t new_space) {
  if (new_space <= space_) return;  // never shrink
  int* new_elements = new int[new_space];
  if (elements_) {
    std::copy(elements_, elements_ + size_, new_elements);
    delete[] elements_;
  }
  elements_ = new_elements;
  space_ = new_space;
}

void IntVector::push_back(int value) {
  if (space_ == 0) {
    resize(8);  // default size is 8
  } else if (space_ == size_) {
    resize(space_ * 2);
  }
  elements_[size_] = value;
  ++size_;
}

int IntVector::pop_back() {
  if (!size_) {
    std::cerr << "ERROR: cannot pop element of empty Vector" << std::endl;
    return 0;
  }
  --size_;
  return elements_[size_];
}

}
