#include <iostream>

#include "point.h"

namespace draw {

class Circle {
public:
  Circle(Point& center, double radius) : radius_(radius), center_(center) {};
  void draw() { std::cout << "drawing Circle(" << center_.x() << "/"
    << center_.y() << ", " << radius_ << ")" << std::endl; }
private:
  double radius_ = 1.0;
  Point& center_;  // references cannot be reassigned; use Point* if needed
  // assigning a new value to a reference would use copy assignment of the type
};

}  // namespace draw

int main(void) {
  // developer has to ensure the point stays in scope
  draw::Point center{17, -2};
  draw::Circle c(center, 5);
  draw::Circle c2(center, 12);  // center can be shared
  c.draw();
  c2.draw();
}
