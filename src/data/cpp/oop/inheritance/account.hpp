#ifndef ACCOUNT_HPP
#define ACCOUNT_HPP

#include <iostream>
#include <string>

namespace bank {

class Account {
public:
  explicit Account(std::string owner) : Account(owner, 0l) {}
  explicit Account(std::string owner, unsigned int deposit);
  void deposit(unsigned int amount);
  virtual unsigned int withdraw(unsigned int amount);
  long long balance() const { return balance_; }
  unsigned long number() const { return number_; }
  std::string owner() const { return owner_; }
private:
  long long balance_ = 0;
  std::string owner_;
  unsigned long number_;
  static unsigned long next_number_;
};


std::ostream& operator<<(std::ostream& out, const Account& a);






}  // namespace bank

#endif  // ACCOUNT_HPP
