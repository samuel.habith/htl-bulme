#include <iostream>

// default constructor: a ctor which can be called with no arguments
class Example {
public:
  Example() {};
  Example(int a) : a_{a} {};
  int a() const { return a_; }
private:
  int a_ = 42;
};

int main() {
  Example e;  // works, because we have defined a default constructor
  std::cout << e.a() << std::endl;
  Example e2{1};
  std::cout << e2.a() << std::endl;
}
