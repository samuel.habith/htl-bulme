#include <iostream>
using namespace std;
class Example {
public:
  int a() const { return a_; }
private:
  int a_ = 42;
};

int main() {
  Example e;  // use compiler generated default constructor
  std::cout << e.a() << std::endl;
}
