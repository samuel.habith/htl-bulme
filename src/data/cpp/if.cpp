#include <iostream>

int main() {
  int guess = 5;  // assume it was entered by a user
  if (guess == 4) {  // guaranteed random guess, used a die
    std::cout << "You won!" << std::endl;
  } else {
    std::cout << "Better luck next time, loser!" << std::endl;
  }
  return 0;
}
