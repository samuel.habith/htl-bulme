#include <iostream>

int main(void) {
    int j = 3;
    int* i = &j;  // C-style pointer
    int& m = j;  // C++ reference
    int& n = m;  // also references j
    n = 2;

    std::cout << "i: " << (*i) << std::endl;
    std::cout << "m: " << m << std::endl;
    std::cout << "n: " << n << std::endl;
}
