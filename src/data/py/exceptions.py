#!/usr/bin/env python3

length = 3
rectangle_area = 12
# ...
try:
    width = rectangle_area / length
except ZeroDivisionError as err:
    print("The length of a rectangle must not be 0")
    # deal with problem or re-raise the exception
    raise
print(width)
