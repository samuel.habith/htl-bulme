#!/usr/bin/env python3

LANG = 'en'
# ...
if LANG == 'en':
    output = "Good Morning!"
elif LANG == 'de':
    output = "Guten Morgen!"
else:
    output = "Bonjour!"
print(output)
