#!/usr/bin/env python3

SECRET = 4  # guaranteed random - chosen via rolling a die

while True:
    TEXT = input("Guess an integer: ")
    NUMBER = int(TEXT)

    if NUMBER == SECRET:
        print("you win")
        break
    elif NUMBER < SECRET:
        print("you guessed too low")
    elif NUMBER > SECRET:
        print("you guessed too high")
