#!/usr/bin/env python3

amount = 3000  # initial investment in euro
GOAL = 4000
INTEREST = 0.03  # 3%

years = 0
while amount < GOAL:
    years += 1
    amount = amount * (1 + INTEREST)

print(f"you'll reach your investment goal after {years} years")
