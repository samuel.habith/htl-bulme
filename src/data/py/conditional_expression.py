#!/usr/bin/env python3

LANG = 'en'
# ...
output = "Good Morning!" if LANG == 'en' else "Guten Morgen!"
print(output)
