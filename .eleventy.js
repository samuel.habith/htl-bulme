const mathjax = require("eleventy-plugin-mathjax");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
const fs = require('fs');
const path = require('path');
const pluginTOC = require('eleventy-plugin-toc');
const pluginNavigation = require("@11ty/eleventy-navigation");

const archivePath = 'https://gitlab.com/senarclens/htl-bulme/-/archive/master/htl-bulme-master';
const pythonTutor = 'http://pythontutor.com/';
/* sample url
https://pythontutor.com/c.html#code=int%20main%28%29%20%7B%0A%0A%20%20return%200%3B%0A%7D&curInstr=0&mode=display&origin=opt-frontend.js&py=c_gcc9.3.0&rawInputLstJSON=%5B%5D */
const language = {
  c: {
    name: 'iframe-embed.html', // 'c.html' has buttons for url generation
    version: 'c_gcc9.3.0',
  },
  cpp: {
    name: 'iframe-embed.html', // 'cpp.html' has buttons for url generation
    version: 'cpp_g++9.3.0',
  },
  py: {
    name: 'iframe-embed.html',
    version: '3',
  },
};

// generate a unique string
const uid = function(){
  return Date.now().toString(36) + Math.random().toString(36).slice(2);
}

module.exports = (config) => {
  // Enable the syntax highlighting plugin
  config.addPlugin(syntaxHighlight);
  config.addPlugin(mathjax);
  config.addPlugin(pluginTOC);
  config.addPlugin(pluginNavigation);
  // Copy the `img` and `styles` folders to the output
  config.addPassthroughCopy('src/images');
  config.addPassthroughCopy('src/styles');
  config.addPassthroughCopy('src/scripts');
  config.addPassthroughCopy('src/data');

  config.addShortcode('year', () => `${new Date().getFullYear()}`);

  config.addShortcode('isodate', (date) => date.toISOString().substring(0, 10));
  config.addNunjucksShortcode('include_code', (filename, startAt=0) => {
    const data = fs.readFileSync(
      path.join(__dirname, 'src/_includes', filename), 'utf8'
    );
    const extension = filename.split('.').pop();
    const code = encodeURIComponent(data).replace(/'/g, '%27');
    const lang = encodeURIComponent(language[extension].version);
    const url = `${pythonTutor}${language[extension].name}#code=${code}&curInstr=${startAt}&mode=display&py=${lang}`;
    return `<p><a href="${url}" target="${filename}" onclick="(function () {console.log('trying to create reference');if (!('tutor_references' in window)) { window.tutor_references = {}; } const tab = window.tutor_references['${filename}']; if (tab?.closed === false) { tab.focus(); } else { window.tutor_references['${filename}'] = window.open('${url}', '${filename}'); } })();return false;// })(); return false;">${filename}</a></p>`;
  });
  config.addNunjucksShortcode('download', (filepath) => {
    const filename = filepath.split('/').pop();
    return `Download <a href="${filepath}">${filename}</a>`;
  });
  config.addPairedNunjucksShortcode(
    'answer',
    (content) => (`<div class="answer"><div class="solution">
      ${content}
      </div></div>`),
  );

  /* Yes / no questions
   * Only supports even numbers of questions.
   */
  config.addPairedNunjucksShortcode('YesNoTable',
    (content, count, preamble='', total_points=count / 2) => {
      const points_per_counted = total_points / count * 2;
      let point_s = 'point';
      if (points_per_counted > 1) point_s = 'points';
      let schema = `<div class="grading">0-${count / 2} correct: 0 points`;
      schema += `, ${count / 2 + 1} correct: ${points_per_counted} ${point_s}`;
      for (let i = count / 2 + 2; i <= count; i += 1) {
        let points = (i - count / 2) * points_per_counted;
        schema += `, ${i} correct: ${points} points`;
      }
      schema += '.</div>';
      return `Answer the following statements indicating whether they are
<strong>True</strong> or <strong>False</strong>.<br />
    ${schema}
    ${preamble}
<table class="yes_no">
  <tr>
    <th>Statement</th>
    <th>True</th>
    <th>False</th>
  </tr>
  ${content}
</table>`});
  config.addNunjucksShortcode('yes_no_question', (question, answer) => {
    let s = `<tr class="yes_no_question"><td>${question}</td>`;
    if (answer.toLowerCase() === 'yes' || answer.toLowerCase() === 'y') {
      s += '<td class="yes_no_solution"></td><td></td>';
    } else {
      s += '<td></td><td class="yes_no_solution"></td>';
    }
    return s;
  });

  /* Multiple choice questions
   * Question text and `mcChoice`s are all in the content.
   */
  config.addPairedNunjucksShortcode(
    'mcQuestion',
    (content) => (`<div class="mc_question">
      ${content}
      <input type="button" value="Toggle solution" class="solution-button">
      </div>`),
  );

  config.addPairedNunjucksShortcode('mcChoice', (answer, isCorrect) => {
    const id = uid();
    return (
      `<div class="choice ${isCorrect}">
         <input type="checkbox"
                name="${id}"
                id="${id}" />
         <label for="${id}" class="${isCorrect}"><div>
           ${answer}
           </div>
         </label>
      </div>`);
  });

  /* Provide links allowing to download entire directories as archives. */
  config.addNunjucksShortcode('archive', (directory, type = 'tar.bz2') => {
    // remove leading and trailing slashes
    let d = directory.endsWith('/') ? directory.slice(0, -1) : directory;
    d = d.startsWith('/') ? d.slice(1) : d;
    const archiveType = type.startsWith('.') ? type.slice(1) : type;
    const url = `${archivePath}.${archiveType}?path=src/${d}`;
    return `<a href="${url}">${d}</a>`;
  });

  config.setTemplateFormats([
    'njk',
    'md',
  ]);
  return {
    dir: {
      input: 'src',
      output: 'public',
    },
    markdownTemplateEngine: 'njk',
  };
};
